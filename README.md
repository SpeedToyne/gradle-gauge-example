# Gradle Gauge Example

This project shows how to create a Gauge Project that is executed via CI/CD through gradle.

If you're looking for automated testing principles, you should look at the [PRINCIPLES.md](./PRINCIPLES.md) file.

You can see the [Latest Report Here](https://gitlab.com/rice-patrick/gradle-gauge-example/-/jobs/artifacts/master/browse?job=run_gauge)

## Quick Start
For this to work, you must have gauge installed. If you don't have gauge installed, install it with the following:
`curl -SsL https://downloads.gauge.org/stable | sh`

Copy and paste this command:
```bash
git clone https://gitlab.com/rice-patrick/gradle-gauge-example.git
cd ./gradle-gauge-example/
./docker-utils/start-local-services.sh

export GAUGE_HOST_PROTOCOL=http

./gradlew gauge
```

## Getting Started 
To execute gauge tests, you will need an instance of the [Oobleck](https://gitlab.com/rice-patrick/oobleck) project 
to test with. You can either download a postgres container and an oobleck container image and run them, or clone the
Oobleck repository and run `./docker-utils/start-local-services.sh && ./gradlew bootRun`

Once Oobleck is running, set your local testing environment to use http by typing `export GAUGE_HOST_PROTOCOL=http` and
run `./gradlew gauge`. 


### Running the pipeline
Since GitLab's "services" don't allow for cross-service communication, we use DIND to run both postgres and oobleck within
docker containers in the CI pipeline. A fresh instance is created with every pipeline, so the database will be empty
every time.

## Getting Started with Gradle
Several of the classes here are useful as a "base" setup of selenium gauge projects, namely the [DriverCache](./gauge-project/src/test/java/org/singleservingsoftware/cache/DriverCache.java)
class and the [SeleniumHook](./gauge-project/src/test/java/org/singleservingsoftware/hook/SeleniumHook.java) classes.

If you want to import those as gradle dependencies, then you can receive all the updates or bug fixes made to them as
part of this project. Since these are test classes and the jars are stored on GitLab, you'll need a couple steps to 
import them.

1. Add the GitLab maven repository for this project to your build script (build.gradle)
```groovy
repositories {
    maven { url "https://gitlab.com/api/v4/projects/13748607/packages/maven" }
}
```

2. Add a dependency to the **sources** jar for this project to your build script.
```groovy
compile 'org.singleservingsoftware:gradle-gauge-starter:0.1.+:sources'
```

And you're done! The SeleniumHook will be found and used automatically.

## Project Setup
This project contains one main sub-project that holds all the gauge tests. Per best-practices for gauge, 
the tests are all contained within the "test" folder (a main set is defined, but not used). There are a 
couple noteworthy pieces of this project:

### `gauge-project/specs/` folder
This folder contains all the [Gauge Specifications](https://docs.gauge.org/latest/writing-specifications.html#specifications-spec)
that define the tests.  There are three specifications right now with very basic tests. One tests that the heartbeat of 
the application functions properly, and the other tests that the basic CRUD operations of legal entity function properly.

The last specification is special, and tagged with "Selenium". See the "Selenium" header below for more information.

### Implementation classes
Classes within the [implementations](./gauge-project/src/test/java/com/singleserving/implementations) package provide 
the implementation of the text steps located within the specifications files. These are implemented by the @Step annotation
as documented by the [Gauge documentation](https://docs.gauge.org/writing-specifications.html#step-implementations). Each 
step implements a small, repeatable piece of a test. As a general rule, one "action" ("add an address", or "click a button")
should be a single implementation. If you can't read the test from top to bottom and know what it's doing without looking at
the implementations, you're doing it wrong.

### Selenium Tests
Tests that are tagged with "selenium" (either at the specification or scenario level) have special logic that runs at 
the end of the test. When accessing the web driver the static DriverCache class using `DriverCache.getDriver()` 
the driver will be created lazily (on first use), and when the test completes the driver in this 
cache will be used to indicate "success" or "failure" if running in SauceLabs. The driver can be used to navigate 
through pages and click on items. At the completion of the scenario, this driver will be destroyed automatically, 
creating a new session for each test.

As well, through the use of [WebDriverManager](https://github.com/bonigarcia/webdrivermanager) the selenium driver
will attempt to automatically detect which browser version you're running and download the driver automatically. This
will default to Chrome is no browser version is passed in via the "SELENIUM_BROWSER" environment variable.

### Using [SauceLabs](https://saucelabs.com/)
The [SeleniumHook](./gauge-project/src/test/java/com/singleserving/hook/SeleniumHook.java) class that implements the
automatic creation and destruction of WebDrivers supports the use of SauceLabs to run selenium tests. At the completion
of a test, it will send a note on the web driver to notify SauceLabs of test success or failure, and it will use the 
name of the scenario (the double hash `##` in the spec file) to name the test in SauceLabs. 

To run tests in SauceLabs, you must provide your SauceLabs account in the `SAUCE_USERNAME` environment variable, and you
must provide your SauceLabs API TOKEN in the `SAUCE_PASSWORD` environment variable. Right now, SauceLabs execution will
always use the Windows10 operating system and Chrome browser. Soon, these will be configurable using the "environment"
options in gauge.


