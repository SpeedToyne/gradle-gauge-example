#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Start docker-compose with the compose file in detached mode
docker-compose -f $DIR/services.yml up -d
