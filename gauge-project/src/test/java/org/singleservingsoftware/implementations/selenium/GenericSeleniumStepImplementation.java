package org.singleservingsoftware.implementations.selenium;

import org.singleservingsoftware.cache.DriverCache;
import com.thoughtworks.gauge.Step;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;

public class GenericSeleniumStepImplementation {


    /**
     * Loads a page into the Driver that is cached
     *
     * @param page The URL to load
     */
    @Step("Navigate to <page>")
    public void navigateToPage(String page) {
        if (DriverCache.getDriver() == null)
            Assertions.fail("No driver loaded. Did you forget to tag your test with the 'selenium' tag?");

        DriverCache.getDriver().get(page);
    }

    /**
     * Check that the Kingland logo is present on the page
     */
    @Step("Verify that the logo is visible")
    public void logoCheck() {
        if (DriverCache.getDriver() == null)
            Assertions.fail("No driver loaded. Did you forget to tag your test with the 'selenium' tag?");

        Assertions.assertTrue(
                DriverCache.getDriver().findElement(By.cssSelector("img.hs-image-widget")).isDisplayed(),
                "Logo is not visible on the page!");
    }

    @Step("Fail the test")
    public void implementation1() {
        Assertions.fail();
    }
}
