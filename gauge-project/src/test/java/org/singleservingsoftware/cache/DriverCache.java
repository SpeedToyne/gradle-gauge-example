package org.singleservingsoftware.cache;

import com.thoughtworks.gauge.ExecutionContext;
import org.openqa.selenium.WebDriver;
import com.thoughtworks.gauge.datastore.*;
import org.singleservingsoftware.hook.SeleniumHook;
import org.singleservingsoftware.utils.SeleniumDriverUtils;

import java.util.logging.Logger;

/**
 * Acts as a mechanism for passing the WebDriver around between StepImplementations
 */
public class DriverCache {

    private static Logger logger = Logger.getLogger(DriverCache.class.getName());

    /**
    * A specification level datastore from gauge. Data stored here is isolated to our current scenario.
    */
    private static DataStore specStore = DataStoreFactory.getScenarioDataStore();

    public static String getSessionID() {
        return (String) specStore.get("sessionId");
    }

    public static void setSessionID(String sessionID) {
        specStore.put("sessionId", sessionID);
    }

    public static WebDriver getDriver() {

        //If the specStore doesn't have a driver, then initialize one.
        if(specStore.get("driver") == null)
        {
            ExecutionContext context = (ExecutionContext) specStore.get("context");

            if(System.getenv("SAUCE_USERNAME") != null && System.getenv("SAUCE_PASSWORD") != null) {
                DriverCache.setDriver(SeleniumDriverUtils.getSauceLabsWebDriver(context));
            }
            else {
                logger.warning("No SauceLabs credentials present. Check SAUCE_USERNAME and SAUCE_PASSWORD if you intend" +
                        "to be running this test on SauceLabs. Defaulting to Local WebDriver");
                DriverCache.setDriver(SeleniumDriverUtils.getLocalWebDriver());
            }
        }

        //At this point, the driver exists, so return it
        return (WebDriver) specStore.get("driver");
    }

    public static void setDriver(WebDriver driver) {
        specStore.put("driver", driver);
    }
}
