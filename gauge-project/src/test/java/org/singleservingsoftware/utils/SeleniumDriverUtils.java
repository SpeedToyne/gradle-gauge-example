package org.singleservingsoftware.utils;

import com.thoughtworks.gauge.ExecutionContext;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.singleservingsoftware.cache.DriverCache;
import org.singleservingsoftware.hook.SeleniumHook;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

/**
 * Extraction of utilities for creating a WebDriver context, either locally or for running on SauceLabs.
 */
public class SeleniumDriverUtils {

    private static Logger logger = Logger.getLogger(SeleniumDriverUtils.class.getName());

    /**
     * The URL to use for saucelabs. Really shouldn't ever change.
     */
    private static String sauceURL = "https://ondemand.saucelabs.com/wd/hub";

    /**
     * Creates a local WebDriver using a configured browser to determine which driver to run. Driver is configured
     * using the "SELENIUM_BROWSER" environment variable
     *
     * If no driver is
     * @return
     */
    public static WebDriver getLocalWebDriver() {
        String browser = System.getenv("SELENIUM_BROWSER");

        if(browser == null)
        {
            logger.fine("No browser configured, defaulting to Chrome. Change using the SELENIUM_BROWSER env variable.");
            browser = "chrome";
        }

        WebDriver toReturn = null;
        switch(browser) {
            case "firefox":
                WebDriverManager.firefoxdriver().setup();
                toReturn = new FirefoxDriver();
                break;

            case "edge":
                WebDriverManager.edgedriver().setup();
                toReturn = new EdgeDriver();
                break; // I mean, it's edge. It probably will.

            default:
                WebDriverManager.chromedriver().setup();
                toReturn = new ChromeDriver();
        }

        return toReturn;
    }

    /**
     * Creates the SauceLabs web driver to use for running tests
     *
     * Most of this code is pulled from here: https://wiki.saucelabs.com/display/DOCS/Java+Test+Setup+Example
     * @return
     *  A WebDriver pointing to SauceLabs
     */
    public static WebDriver getSauceLabsWebDriver(ExecutionContext context) {

        MutableCapabilities sauceOpts = new MutableCapabilities();
        sauceOpts.setCapability("username", System.getenv("SAUCE_USERNAME") );
        sauceOpts.setCapability("accessKey", System.getenv("SAUCE_PASSWORD"));
        sauceOpts.setCapability("seleniumVersion", "3.141.59");
        sauceOpts.setCapability("name",
                "(" + context.getCurrentSpecification().getName() + ")" + context.getCurrentScenario().getName());

        //Add our tags to the options so we can sort by them in SauceLabs later if we want
        sauceOpts.setCapability("tags", context.getAllTags());

        //check if we're running on a CI build, and use that as the build name if we are. Otherwise set to "local"
        // and use a timestamp


        if(System.getenv("CI_JOB_ID") != null) { //Check for GitLab
            sauceOpts.setCapability("build", "GitLab-" + System.getenv("CI_JOB_ID"));
        }
        else if (System.getenv("JOB_NAME") != null) { //Check for Jenkins
            sauceOpts.setCapability("build", "Jenkins-" + System.getenv("JOB_NAME") + "_" + System.getenv("BUILD_NUMBER"));
        }
        else { //Default to local
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
            sauceOpts.setCapability("build", "Local-" + format.format(new Date()));
        }

        /** Required to set w3c protoccol **/
        ChromeOptions chromeOpts = new ChromeOptions();
        chromeOpts.setExperimentalOption("w3c", true);

        /** Set a second MutableCapabilities object to pass Sauce Options and Chrome Options **/
        MutableCapabilities capabilities = new MutableCapabilities();
        capabilities.setCapability("sauce:options", sauceOpts);
        capabilities.setCapability("goog:chromeOptions", chromeOpts);
        capabilities.setCapability("browserName", "chrome");
        capabilities.setCapability("platformVersion", "Windows 10");
        capabilities.setCapability("browserVersion", "latest");

        RemoteWebDriver driver = null;
        try {
            driver = new RemoteWebDriver(new URL(sauceURL),capabilities);
            driver.setFileDetector(new LocalFileDetector());
        } catch (MalformedURLException e) {
            logger.severe("Unable to create webdriver - bad saucelabs URL");
            throw new RuntimeException(e);
        }

        DriverCache.setSessionID(driver.getSessionId().toString());

        return driver;
    }


}
