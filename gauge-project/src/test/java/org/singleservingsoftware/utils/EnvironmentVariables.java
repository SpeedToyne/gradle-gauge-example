package org.singleservingsoftware.utils;


/**
 * Class for handling configurations for the tests
 */
public class EnvironmentVariables {


    /**
     * Returns the hostname value, or "localhost" if it's not set
     * @return
     *  The hostname value
     */
    public static String getBaseHostname() {
        String toReturn = System.getenv("GAUGE_HOST_ADDRESS");
        //Set default value if one isn't present
        toReturn = (toReturn == null ? "localhost" : toReturn);

        return toReturn;
    }

    /**
     * Returns the port value, or "8080" if it's not set
     * @return
     *  The port value
     */
    public static String getBasePort() {
        String toReturn = System.getenv("GAUGE_HOST_PORT");
        //Set default value if one isn't present
        toReturn = (toReturn == null ? "8080" : toReturn);

        return toReturn;
    }

    /**
     * Returns the protocol value, or "https" if it's not set
     * @return
     *  The protocol value
     */
    public static String getBaseProtocol() {
        String toReturn = System.getenv("GAUGE_HOST_PROTOCOL");
        //Set default value if one isn't present
        toReturn = (toReturn == null ? "https" : toReturn);

        return toReturn;
    }
}
