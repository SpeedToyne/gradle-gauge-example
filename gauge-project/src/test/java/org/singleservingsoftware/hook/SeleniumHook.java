package org.singleservingsoftware.hook;

import com.thoughtworks.gauge.Gauge;
import com.thoughtworks.gauge.datastore.DataStoreFactory;
import org.junit.jupiter.api.parallel.Execution;
import org.singleservingsoftware.cache.DriverCache;
import com.thoughtworks.gauge.AfterScenario;
import com.thoughtworks.gauge.BeforeScenario;
import com.thoughtworks.gauge.ExecutionContext;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.singleservingsoftware.utils.SeleniumDriverUtils;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Driver;
import java.util.Date;
import java.util.logging.Logger;

public class SeleniumHook {

    private static Logger logger = Logger.getLogger(SeleniumHook.class.getName());

    /**
     * Stores the executionContext of the current test within the scenario store. This allows other tests to access
     * it easily. Note that this runs on _all_ tests, not just selenium tests.
     * @param context
     */
    @BeforeScenario()
    public void setupContext(ExecutionContext context)
    {
        //Store the context in the scenario store for lazy loading later on
        DataStoreFactory.getScenarioDataStore().put("context", context);

        //The rest of the driver will be lazy loaded on first use - see DriverCache.getDriver()
    }

    /**
     * Runs after selenium tests to store the results of the test if running in SauceLabs. Limited to running on tests
     * tagged with _selenium_
     * @param context
     */
    @AfterScenario(tags = {"selenium"})
    public void teardownSeleniumDriver(ExecutionContext context) {

        if(DriverCache.getDriver() == null)
        {
            String s = "No driver was present when closing the test. If using a remote driver, test" +
                    "status may not be persisted correctly! This is likely a coding error";

            //Log this in several places (in the logs, and in the report
            logger.warning(s);
            Gauge.writeMessage(s);
        }


        //Set the job status using a Javascript executor if we're running on SauceLabs
        if(DriverCache.getDriver().getClass().isAssignableFrom(RemoteWebDriver.class))
        {
            String jobStatus = (context.getCurrentScenario().getIsFailing() ? "failed" : "passed");
            ((JavascriptExecutor) DriverCache.getDriver()).executeScript("sauce:job-result=" + jobStatus);

            if(DriverCache.getSessionID() != null && jobStatus.equals("failed"))
                Gauge.writeMessage(
                        String.format("Sauce URL: https://app.saucelabs.com/tests/%s",
                                DriverCache.getSessionID()));
        }


        //Close the driver
        if(DriverCache.getDriver() != null)
            DriverCache.getDriver().quit();
    }


}
