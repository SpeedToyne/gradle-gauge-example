package org.singleservingsoftware.hook;

import com.thoughtworks.gauge.BeforeScenario;
import com.thoughtworks.gauge.ExecutionContext;
import com.thoughtworks.gauge.datastore.DataStore;
import com.thoughtworks.gauge.datastore.DataStoreFactory;
import org.singleservingsoftware.cache.DriverCache;

/**
 * This class just adds the scenario and specification name to a scenario-level datastore for every test,
 * so that they can be retrieved later if necessary for creating unique testing data.
 */
public class SpecNameHook {

    /**
     * A specification level datastore from gauge. Data stored here is isolated to our current scenario.
     */
    private static DataStore specStore = DataStoreFactory.getScenarioDataStore();

    /**
     * This runs before every scenario, and adds data to the spec store.
     * @param context
     */
    @BeforeScenario
    public void addScenarioName(ExecutionContext context) {

        specStore.put("scenarioName", context.getCurrentScenario().getName());
        specStore.put("specificationName", context.getCurrentSpecification().getName());

    }
}
