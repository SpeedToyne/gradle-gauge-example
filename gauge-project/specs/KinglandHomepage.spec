# Sample Selenium Test
Tags: selenium

Executes an example Selenium test

## Test that the Kingland logo is visible
* Navigate to "http://www.kingland.com"
* Verify that the logo is visible
