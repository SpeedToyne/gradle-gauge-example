
# LegalEntityAddress

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**addressOne** | **String** |  |  [optional]
**addressTwo** | **String** |  |  [optional]
**addressType** | **Long** |  |  [optional]
**city** | **String** |  |  [optional]
**country** | **Long** |  |  [optional]
**lastUpdateDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**lastUpdateReason** | **String** |  |  [optional]
**lastUpdateTransaction** | **String** |  |  [optional]
**legalEntity** | [**LegalEntity**](LegalEntity.md) |  |  [optional]
**legalEntityAddressId** | **Long** |  |  [optional]
**legalEntityId** | **Long** |  |  [optional]
**state** | **Long** |  |  [optional]



