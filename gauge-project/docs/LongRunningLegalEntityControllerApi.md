# LongRunningLegalEntityControllerApi

All URIs are relative to *https://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getLegalEntityByIdUsingGET1**](LongRunningLegalEntityControllerApi.md#getLegalEntityByIdUsingGET1) | **GET** /legalEntity/stream/ | Retrieves a set of LegalEntities that returns the object as soon as it returns


<a name="getLegalEntityByIdUsingGET1"></a>
# **getLegalEntityByIdUsingGET1**
> ResponseBodyEmitter getLegalEntityByIdUsingGET1(id)

Retrieves a set of LegalEntities that returns the object as soon as it returns

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.LongRunningLegalEntityControllerApi;


LongRunningLegalEntityControllerApi apiInstance = new LongRunningLegalEntityControllerApi();
Integer id = 56; // Integer | the ID to retrieve
try {
    ResponseBodyEmitter result = apiInstance.getLegalEntityByIdUsingGET1(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LongRunningLegalEntityControllerApi#getLegalEntityByIdUsingGET1");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| the ID to retrieve |

### Return type

[**ResponseBodyEmitter**](ResponseBodyEmitter.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

