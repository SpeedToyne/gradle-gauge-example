
# LegalEntityIdentifier

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identifierType** | **Long** |  |  [optional]
**identifierValue** | **String** |  |  [optional]
**lastUpdateDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**lastUpdateReason** | **String** |  |  [optional]
**lastUpdateTransaction** | **String** |  |  [optional]
**legalEntity** | [**LegalEntity**](LegalEntity.md) |  |  [optional]
**legalEntityId** | **Long** |  |  [optional]
**legalEntityIdentifierId** | **Long** |  |  [optional]



