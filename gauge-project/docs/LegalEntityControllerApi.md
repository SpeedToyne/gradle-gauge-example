# LegalEntityControllerApi

All URIs are relative to *https://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getLegalEntityByIdUsingGET**](LegalEntityControllerApi.md#getLegalEntityByIdUsingGET) | **GET** /legalEntity | Retrieves a LegalEntityModel by it&#39;s ID
[**insertLegalEntityAddressUsingPOST**](LegalEntityControllerApi.md#insertLegalEntityAddressUsingPOST) | **POST** /legalEntity/address | Performs an insert of a Legal Entity Address Object.
[**insertLegalEntityUsingPOST**](LegalEntityControllerApi.md#insertLegalEntityUsingPOST) | **POST** /legalEntity | Inserts a new LegalEntity into the database
[**persistLegalEntityUsingPUT**](LegalEntityControllerApi.md#persistLegalEntityUsingPUT) | **PUT** /legalEntity | Performs a full update of a Legal Entity. 
[**updateLegalEntityUsingPATCH**](LegalEntityControllerApi.md#updateLegalEntityUsingPATCH) | **PATCH** /legalEntity | Performs a partial update of a Legal Entity. 


<a name="getLegalEntityByIdUsingGET"></a>
# **getLegalEntityByIdUsingGET**
> LegalEntity getLegalEntityByIdUsingGET(id)

Retrieves a LegalEntityModel by it&#39;s ID

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.LegalEntityControllerApi;


LegalEntityControllerApi apiInstance = new LegalEntityControllerApi();
Long id = 789L; // Long | the ID to retrieve
try {
    LegalEntity result = apiInstance.getLegalEntityByIdUsingGET(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LegalEntityControllerApi#getLegalEntityByIdUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| the ID to retrieve |

### Return type

[**LegalEntity**](LegalEntity.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

<a name="insertLegalEntityAddressUsingPOST"></a>
# **insertLegalEntityAddressUsingPOST**
> LegalEntityAddress insertLegalEntityAddressUsingPOST(address)

Performs an insert of a Legal Entity Address Object.

Will throw a 500 error if the legal_entity_id is empty.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.LegalEntityControllerApi;


LegalEntityControllerApi apiInstance = new LegalEntityControllerApi();
LegalEntityAddress address = new LegalEntityAddress(); // LegalEntityAddress | address
try {
    LegalEntityAddress result = apiInstance.insertLegalEntityAddressUsingPOST(address);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LegalEntityControllerApi#insertLegalEntityAddressUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **address** | [**LegalEntityAddress**](LegalEntityAddress.md)| address |

### Return type

[**LegalEntityAddress**](LegalEntityAddress.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="insertLegalEntityUsingPOST"></a>
# **insertLegalEntityUsingPOST**
> LegalEntity insertLegalEntityUsingPOST(entity)

Inserts a new LegalEntity into the database

If an ID is provided, it will be ignored by the default implementation

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.LegalEntityControllerApi;


LegalEntityControllerApi apiInstance = new LegalEntityControllerApi();
LegalEntity entity = new LegalEntity(); // LegalEntity | entity
try {
    LegalEntity result = apiInstance.insertLegalEntityUsingPOST(entity);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LegalEntityControllerApi#insertLegalEntityUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **entity** | [**LegalEntity**](LegalEntity.md)| entity |

### Return type

[**LegalEntity**](LegalEntity.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="persistLegalEntityUsingPUT"></a>
# **persistLegalEntityUsingPUT**
> LegalEntity persistLegalEntityUsingPUT(entity)

Performs a full update of a Legal Entity. 

If an attribute is passed in as null, it will be nulled in the database. Any one-to-many objects not passed in will be removed.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.LegalEntityControllerApi;


LegalEntityControllerApi apiInstance = new LegalEntityControllerApi();
LegalEntity entity = new LegalEntity(); // LegalEntity | entity
try {
    LegalEntity result = apiInstance.persistLegalEntityUsingPUT(entity);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LegalEntityControllerApi#persistLegalEntityUsingPUT");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **entity** | [**LegalEntity**](LegalEntity.md)| entity |

### Return type

[**LegalEntity**](LegalEntity.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="updateLegalEntityUsingPATCH"></a>
# **updateLegalEntityUsingPATCH**
> LegalEntity updateLegalEntityUsingPATCH(entity)

Performs a partial update of a Legal Entity. 

Any null attributes will be ignored during the update process. Any one-to-many objects not passed in will be ignored (determined by ID)

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.LegalEntityControllerApi;


LegalEntityControllerApi apiInstance = new LegalEntityControllerApi();
LegalEntity entity = new LegalEntity(); // LegalEntity | entity
try {
    LegalEntity result = apiInstance.updateLegalEntityUsingPATCH(entity);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LegalEntityControllerApi#updateLegalEntityUsingPATCH");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **entity** | [**LegalEntity**](LegalEntity.md)| entity |

### Return type

[**LegalEntity**](LegalEntity.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

