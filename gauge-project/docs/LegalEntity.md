
# LegalEntity

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**addressList** | [**List&lt;LegalEntityAddress&gt;**](LegalEntityAddress.md) |  |  [optional]
**id** | **Long** |  |  [optional]
**identifierList** | [**List&lt;LegalEntityIdentifier&gt;**](LegalEntityIdentifier.md) |  |  [optional]
**lastUpdateDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**lastUpdateReason** | **String** |  |  [optional]
**lastUpdateTransaction** | **String** |  |  [optional]
**nameList** | [**List&lt;LegalEntityName&gt;**](LegalEntityName.md) |  |  [optional]



