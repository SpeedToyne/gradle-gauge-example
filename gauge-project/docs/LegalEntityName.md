
# LegalEntityName

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lastUpdateDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**lastUpdateReason** | **String** |  |  [optional]
**lastUpdateTransaction** | **String** |  |  [optional]
**legalEntity** | [**LegalEntity**](LegalEntity.md) |  |  [optional]
**legalEntityId** | **Long** |  |  [optional]
**legalEntityNameId** | **Long** |  |  [optional]
**nameType** | **Long** |  |  [optional]
**nameValue** | **String** |  |  [optional]



