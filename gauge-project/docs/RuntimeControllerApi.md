# RuntimeControllerApi

All URIs are relative to *https://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**heartbeatUsingGET**](RuntimeControllerApi.md#heartbeatUsingGET) | **GET** / | Provides a heartbeat confirming the application is running
[**heartbeatUsingGET2**](RuntimeControllerApi.md#heartbeatUsingGET2) | **GET** /heartbeat | Provides a heartbeat confirming the application is running


<a name="heartbeatUsingGET"></a>
# **heartbeatUsingGET**
> Boolean heartbeatUsingGET()

Provides a heartbeat confirming the application is running

This api will always return true with a 200 status as long as the applicatoin is up and running. It can be used with a load balancer health check or other similar operations.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.RuntimeControllerApi;


RuntimeControllerApi apiInstance = new RuntimeControllerApi();
try {
    Boolean result = apiInstance.heartbeatUsingGET();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RuntimeControllerApi#heartbeatUsingGET");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**Boolean**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

<a name="heartbeatUsingGET2"></a>
# **heartbeatUsingGET2**
> Boolean heartbeatUsingGET2()

Provides a heartbeat confirming the application is running

This api will always return true with a 200 status as long as the applicatoin is up and running. It can be used with a load balancer health check or other similar operations.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.RuntimeControllerApi;


RuntimeControllerApi apiInstance = new RuntimeControllerApi();
try {
    Boolean result = apiInstance.heartbeatUsingGET2();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RuntimeControllerApi#heartbeatUsingGET2");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**Boolean**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

