# Automated Testing Principles
Getting a running project for automated testing is just the first step. Creating tests that represent "maintainable" tests is something that many people struggle with, but is critical to getting tests that will run sustainably. Remember _sustainable testing is key to automated testing_. If your tests are not sustainable and constantly break, other engineers on your team will stop paying attention to them, and worse, stop maintaining them.

This page seeks to layout some basic principles for creating sustainable automated tests.

## Create the data that you need for your tests
Don't rely on data that already exists within the system. You will run into three primary problems if you do this:
1. Data will change between runs, resulting in failed tests because the data isn't present
2. You won't be able to tests border cases, because border-case data may not exist
3. Your tests will not be [idempotent](https://www.restapitutorial.com/lessons/idempotency.html), which means they may pass the first time but they will fail every subsequent time.

"Setup" scripts are good for this in gauge if you want to create the same data for every tests in a scenario:
```md
# Arbitrary Testing Service
tags: data

* Create a new data object
* Add an attribute of "name" to the object with a random value

## Test Saving a Random Object
tags: save

* Save the object
```

## Don't combine steps for brevity
It can be tempting to try and combine multiple steps into a single one to try and reduce the number of steps you need to implement. _Don't do this_. This makes your steps less re-usable, and moves more of the logic into Java, which may be easier to engineer, but is more difficult to maintain later on. You may have multiple steps for saving an object (bad!), and you will undoubtedly end up with multiple steps that do the same thing.

### Example of a Bad Step
```md
# Password Hashing Service
tags: password, user

## Test that a user's password is hashed correctly

* Create a new user object, add a random value to the "password" step, then hash the value and verify
```

### Example of a Better Step
```md
# Password Hashing Service
tags: password, user

## Test that a user's password is hashed correctly

We are going to store the object in the Specification Store, so that we can pass it between steps

* Create a new user object 
* add a random value to the "password" value 
* hash the "password" value
* verify that the "password" value has been hashed.
```
